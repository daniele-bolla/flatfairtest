import React from "react";
import axios from "axios";
const Input = ({ ...props }) => {
  return (
    <fieldset aria-describedby="description">
      <label htmlFor={props.name}>{props.title}</label>
      <input id={props.name} {...props} />
      {props.description ? <p id="description">{props.description}</p> : null}
    </fieldset>
  );
};
const Select = ({ list, ...props }) => {
  return (
    <fieldset aria-describedby="description">
      <label htmlFor={props.name}>{props.title}</label>
      <select id={props.name} {...props}>
        {list.map(item => (
          <option key={item.value} value={item.value}>
            {item.label}
          </option>
        ))}
      </select>
      {props.description ? <p id="description">{props.description}</p> : null}
    </fieldset>
  );
};
const API =
	"https://cxynbjn3wf.execute-api.eu-west-2.amazonaws.com/production/config";
	const pipe = (...fns) => x => fns.reduce((v, f) => f(v), x);
	const trace = label => value => {
		console.log(`${label}: ${value}`);
		return value;
	};
	const vatCalc = cost => (cost / 100) * (20 + 100);
class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      rent_type: 1,
      fixed_membership_fee: null,
      fixed_membership_fee_amount: null,
      membershipFee: null
    };
  }
  componentDidMount() {
    axios
      .get(API)
      .then(result => {
        const {
          data: { fixed_membership_fee, fixed_membership_fee_amount }
        } = result;
        this.setState({
          fixed_membership_fee,
          fixed_membership_fee_amount
        });
      })
      .catch(console.log);
  }
  handleChange = e => {
    e.preventDefault();
    const { name, value } = e.target;
    this.setState(
      () => ({ [name]: value }),
      () => {
        this.membershipFee(this.state, value);
      }
    );
  };

  membershipFee = (state, value) => {
    const { fixed_membership_fee, fixed_membership_fee_amount } = state;
    const calculateFee = week =>
      week <= 120 ? 120 + vatCalc(120) : week + vatCalc(week);
    const extratWeek = month => Math.floor((month * 12) / 52);
    const calculateByMonth = rent =>
      pipe(
        extratWeek,
        calculateAndSetFee,
      )(rent);
    const setFee = membershipFee => this.setState({ membershipFee });
    const calculateAndSetFee = rent =>
      pipe(
        calculateFee,
        setFee,
      )(rent);
    const switchByRentType = ({ rent_type }, rent) => {
      const cases = {
        montlhy: () => calculateByMonth(rent),
        weekly: () => calculateAndSetFee(rent)
      };
      return cases[rent_type]();
    };
    if (fixed_membership_fee) {
      setFee(fixed_membership_fee_amount + vatCalc(fixed_membership_fee_amount));
    } else {
      switchByRentType(state, value);
    }
  };
  render() {
    const list = [
      { value: "weekly", label: "Weekly" },
      { value: "montlhy", label: "Montlhy" }
    ];
    return (
      <form>
        <p>Select Rent Type: {this.state.rent_type}</p>
        <Select name="rent_type" list={list} onChange={this.handleChange} />
        {this.state.rent_type === "weekly" ? (
          <Input
            title="Rent"
            name="rent_weekly"
            type="number"
            placeholder="Weekly"
            min="25"
            max="110"
            aria-required="true"
            onChange={this.handleChange}
          />
        ) : this.state.rent_type === "montlhy" ? (
          <Input
            title="Rent"
            name="rent_montly"
            type="number"
            placeholder="Montly"
            min={2000}
            max={8660}
            aria-required="true"
            onChange={this.handleChange}
          />
        ) : null}
        <Input
          title="Postcode"
          name="postcode"
          type="text"
          placeholder="Postcode"
          aria-required="true"
          onChange={this.handleChange}
        />
        <p>Membership : {this.state.membershipFee}</p>
        <button>Submit</button>
      </form>
    );
  }
}
//

export default Form;
