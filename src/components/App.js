/** @jsx jsx */
import { css, jsx, Global } from "@emotion/core";
import { ThemeProvider } from "emotion-theming";
import Form from "./Form";
import { BrowserRouter as Router, Route } from "react-router-dom";

const hsl = (hue, saturation, lightness) =>
  `hsl(${hue}, ${saturation}%, ${lightness}%)`;

const shadeOfGrey = lightness => hsl(0, 0, lightness);

const theme = {
  colors: {
    primary: "#d6b14d",
    secondary: "#8E6A5E",
    shadeOfGrey_1: shadeOfGrey(11),
    shadeOfGrey_2: shadeOfGrey(13),
    shadeOfGrey_3: shadeOfGrey(15),
    shadeOfGrey_4: shadeOfGrey(16),
    shadeOfGrey_5: shadeOfGrey(27),
    shadeOfGrey_6: shadeOfGrey(33),
    shadeOfGrey_7: shadeOfGrey(35),
    shadeOfGrey_8: shadeOfGrey(60)
  }
};

const typography = css`
  body {
    font-family: "Open Sans", sans-serif;
    background-color: #444;
    color: #fff;
  }
  h1 {
    font-family: "Source Sans Pro", sans-serif;
    color: ${theme.colors.primary};
  }
  a,
  button {
    font-size: 1rem;
    color: #234456;
    text-decoration: none;
  }
  a:hover,
  button:hover {
    color: ${theme.colors.primary};
  }
`;
const wrapper = css`
  color: #eee;
	background-color: ${theme.colors.shadeOfGrey13};
	margin:2rem auto;
	border:solid 1px #222;

`;

const App = () => (
  <ThemeProvider theme={theme}>
    <Global styles={typography} />
    <Body />
  </ThemeProvider>
);

const Wrapper = ({ children }) => <div css={wrapper}>{children}</div>;
const Home = ()=> <h1>Home</h1>
const Topics = ()=> <h1>Topics</h1>
const Body = () => (
  <Wrapper>
			<Router>
				<div>
					<Route exact path="/" component={Form} />
					<Route path="/details" component={Topics} />
				</div>
    </Router>
  </Wrapper>
);

export default App;

