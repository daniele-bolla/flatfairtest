import axios from "axios";
const calculateFee = week =>
week <= 120 ? 120 + vatCalc(120) : week + vatCalc(week);
const vatCalc = cost => (cost / 100) * 20;
const API =
	"https://cxynbjn3wf.execute-api.eu-west-2.amazonaws.com/production/config";
import Form from '../components/Form';

describe('get api', () => {
  it('should load data', () => {
    return axios
		.get(API)
    .then(data => {
      expect(data).toBeDefined()
    })
  })
});
describe('Membership fee', () => {
  it('Membership fee is equal to one week of rent + VAT', () => {
    expect(calculateFee(200)).toBe(240)
	})
	it('Minimum membership fee is £120 + VAT', () => {
    expect(calculateFee(120)).toBe(144)
	})
	/*it('If the fixed_membership_fee is true then the membership fee should be equal to the	fixed_membership_fee_amount + VAT', () => {

      expect(Form.membershipFee({fixed_membership_fee:true, fixed_membership_fee_amount:3000})).toBeDefined()
  })*/
});